db:
	docker run -p 5432:5432 \
			--rm \
			--name postgresdb \
			--network oil-net \
			-e POSTGRES_PASSWORD=password \
			-v db-data:/var/lib/postgressql/data \
			postgres:10.16

backend:
	docker run -p 3001:3001  \
			--rm \
			--name oil-backend \
			--network oil-net \
			-v /Users/a.mayatskiy/projects/work/dts/oil-site/server:/app \
			-v /app/node_modules \
			--env-file ./server/.env \
			oil-backend

frontend:
	docker run -p 4200:4200 \
			--rm \
			--name oil-frontend \
			-v /Users/a.mayatskiy/projects/work/dts/oil-site/client/src:/app/src \
			oil-frontend

stop:
	docker stop mongodb oil-backend oil-frontend

dev:
	docker-compose -f docker-compose.yml up

prod:
	docker-compose -f docker-compose.production.yml up

down:
	docker-compose down
