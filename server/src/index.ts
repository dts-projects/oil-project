import express, { Request, Response } from 'express'
import db from 'db'
const cors =  require('cors')

const PORT = process.env.PORT || 3001

const app = express()

app.use(cors())
// app.use(express.json({ extended: true }))

// app.get('/api/hello', (req: Request, res: Response) => {
//   res.status(200).json('Hi hi hi')
// })

app.route('/api/hello')
    .get((req: Request, res: Response) => {
      res.status(200).json('Hi hi hi')
    });

app.listen(PORT, () => {
  console.log(`app runnin on port ${PORT}`)
  db.runMigrations()
})

