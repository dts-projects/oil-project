import {AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements OnInit, AfterViewInit {
  @ViewChild('nav', {read: ElementRef}) nav: ElementRef;

  currentFragment = '';
  isSticky = false;
  isInverse = false;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.currentFragment = this.route.snapshot.fragment;

    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        const target = this.router.parseUrl(event.url).fragment;
        this.currentFragment = target;

        if (target) {
          this.scrollToSection(target);
        }
      }
    });
  }

  ngAfterViewInit(): void {
    this.setInverseMenu();

    setTimeout(() => {
      this.scrollToSection(this.currentFragment);
    }, 0);
  }

  @HostListener('window:scroll', ['$event'])

  scroll(event) {
    this.setInverseMenu();
    this.removeFragment();
  }

  scrollToSection(target) {
    const section = document.getElementById(target);

    if (section) {
      section.scrollIntoView({
        behavior: 'smooth'
      });
    }
  }

  setStickyMenu() {
    // if (this.nav.nativeElement.getBoundingClientRect().top < -30) {
    //   this.isSticky = true;
    // } else {
    //   this.isSticky = false;
    // }

    // this.isSticky = true;
  }

  setInverseMenu() {
    if (this.nav.nativeElement.getBoundingClientRect().top < -30) {
      this.isInverse = true;
    } else {
      this.isInverse = false;
    }

    // this.isSticky = true;
  }

  removeFragment() {
    if (this.nav.nativeElement.getBoundingClientRect().top === 0) {
      this.router.navigate(['.'], {relativeTo: this.route, queryParams: []});
    }
  }

  openMenu() {
    document.documentElement.classList.add('menu--opened');
    document.documentElement.classList.add('modal--opened');
  }

  closeMenu() {
    document.documentElement.classList.remove('menu--opened');
    document.documentElement.classList.remove('modal--opened');
  }
}
