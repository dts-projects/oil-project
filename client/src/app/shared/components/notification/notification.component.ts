import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent extends BaseComponent implements OnInit {
  @Output() closed: EventEmitter<any> = new EventEmitter<any>();

  public isClosed = false;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('notification');
  }

  close() {
    this.isClosed = true;
    this.closed.emit();
  }
}
