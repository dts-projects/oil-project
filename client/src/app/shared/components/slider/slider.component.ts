import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import Swiper, {Navigation, Pagination} from 'swiper';
import {SwiperComponent} from 'swiper/angular';
import MediaQuery from '../../../core/utils/media';
import SwiperCore from 'swiper';
import {BaseComponent} from "../base/base.component";

SwiperCore.use([Navigation, Pagination]);

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent extends BaseComponent implements OnInit  {
  @ViewChild('swiper', { static: false }) swiper?: SwiperComponent;

  @Input() id: string;
  @Input() data: any;

  public config = null;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('swiper');
  }

  slideNext() {
    this.swiper.swiperRef.slideNext(100);
  }

  slidePrev() {
    this.swiper.swiperRef.slidePrev(100);
  }

  onSwiper($event: any) {
    // console.log('$event', $event);
  }
}
