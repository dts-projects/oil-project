import { Component, OnInit } from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-text-box',
  templateUrl: './text-box.component.html',
  styleUrls: ['./text-box.component.scss']
})
export class TextBoxComponent extends BaseComponent implements OnInit {
  public isOpen = false;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('text-box');
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }
}
