import { Component, OnInit } from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-help-box',
  templateUrl: './help-box.component.html',
  styleUrls: ['./help-box.component.scss']
})
export class HelpBoxComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('help-box');
  }
}
