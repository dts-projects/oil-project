import {Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss']
})
export class MediaComponent extends BaseComponent implements OnInit {
  @Input() data;
  @Input() icon = true;

  public imagesPath = '/assets/images/components/media/images/';

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('media');
  }
}
