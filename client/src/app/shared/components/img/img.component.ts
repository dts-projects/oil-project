import {Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

interface Breakpoints {
  sm: boolean;
  md: boolean;
  lg: boolean;
  xl: boolean;
}

@Component({
  selector: 'app-img',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.scss']
})

export class ImgComponent extends BaseComponent implements OnInit {
  @Input() width: number;
  @Input() height: number;
  @Input() maxWidth: string;
  @Input() name: string;
  @Input() ext = 'jpg';
  @Input() alt: string;
  @Input() breakpoints: Breakpoints;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('img');
  }
}
