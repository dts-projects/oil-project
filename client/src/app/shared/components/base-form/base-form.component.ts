import { Component, OnInit } from '@angular/core';
import {ConfirmModalComponent} from '../../../modals/confirm-modal/confirm-modal.component';
import {SimpleModalService} from 'ngx-simple-modal';
import {Router} from '@angular/router';
import {FormGroup} from '@angular/forms';
import formFieldMeta from '../../../core/form/formFieldMeta';
import fieldError from '../../../core/form/fieldError';
import FormControlName from 'src/app/core/maps/FormControlName';
import {AlertModalComponent} from '../../../modals/alert-modal/alert-modal.component';
import {environment} from '../../../../environments/environment';
import {take} from 'rxjs/operators';
import {CommonService} from '../../../core/services/common/common.service';

@Component({
  selector: 'app-base-form',
  templateUrl: './base-form.component.html',
  styleUrls: ['./base-form.component.scss']
})
export class BaseFormComponent implements OnInit {
  public FormFieldMeta = formFieldMeta;
  public FormControlName = FormControlName;
  public FormFieldError = fieldError;

  public form: FormGroup;
  public isLoading = false;

  constructor(
    public simpleModal: SimpleModalService,
    public router: Router,
    public commonService: CommonService
    ) { }

  ngOnInit(): void {
  }

  onSubmit(data) {
    this.form.markAllAsTouched();

    if (this.form.invalid) {
      return;
    }

    this.isLoading = true;

    this.commonService.sendMail({
      'api-key': environment.api_key,
       ...data
    })
      .pipe(take(1))
      .subscribe(() => {
          this.isLoading = false;
          this.alert('Ваше сообщение отправлено!<br /> Мы свяжемся с Вами в ближайшее время!')
            .pipe(take(1))
            .subscribe(() => {
              this.form.reset();
            });
        },
        (error) => {
          this.isLoading = false;
          this.alert('Ой, что-то пошло не так!<br /> Сообщение не было отправлено!', 'Понятно!')
            .pipe(take(1))
            .subscribe(() => {});
        });
  }

  captchaResolved(value: string) {
    if (!value) {
      this.form.get(FormControlName.Agree).setValue(false);
    }
  }

  executeCaptcha(captcha) {
    captcha.execute();
    this.form.markAllAsTouched();
  }

  confirm(e) {
    e.preventDefault();

    this.simpleModal.addModal(ConfirmModalComponent, {
      message: 'Перейти на страницу "Политика конфиденциальности"?'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.router.navigate([]).then((result) => {
          window.open('/info/privacy-policy', '_blank');
        });
      }
    });
  }

  alert(message, btnText = null) {
    return this.simpleModal.addModal(AlertModalComponent, {
      message,
      btnText
    });
  }
}
