import {Component, Input, OnInit} from '@angular/core';
import {UtilsService} from '../../../core/services/utils.service';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseComponent implements OnInit {
  @Input() data;

  constructor(private utils: UtilsService) {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('list');
  }

  isLink(item) {
    return this.utils.isObject(item) && item.href;
  }
}
