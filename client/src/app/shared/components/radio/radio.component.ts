import {Component, EventEmitter, forwardRef, Input, OnInit, Output} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {ValueAccessorComponent} from '../value-accessor/value-accessor.component';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => RadioComponent),
    multi: true
  }]
})
export class RadioComponent extends ValueAccessorComponent implements OnInit {
  @Output() change: EventEmitter<any> = new EventEmitter<any>();
  @Input() checked: boolean;
  @Input() id: string;
  @Input() data: any;
  @Input() name: string;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('radio');
  }

  changeValue(id) {
    super.changeValue(id);
    this.change.emit(id || this.checked);
  }

  writeValue(value: any): void {
    super.writeValue(value);
    this.change.emit(value);
  }
}
