import {Component, ElementRef, forwardRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {Pattern} from '../../../core/pattern/pattern';
import {ValueAccessorComponent} from '../value-accessor/value-accessor.component';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    }
  ]
})
export class InputComponent extends ValueAccessorComponent implements OnInit, OnChanges {
  @ViewChild('editContent', {read: ElementRef}) editContent: ElementRef;

  @Input() id: string;
  @Input() name: string;
  @Input() type: string;
  @Input() inputmode: string;
  @Input() unit: string;
  @Input() mask: string;
  @Input() prefix = '';
  @Input() suffix = '';
  @Input() placeholder = '';
  @Input() rows = 5;
  @Input() isValid = false;
  @Input() maxlength: number;
  @Input() dropSpecialCharacters: boolean;
  @Input() showMaskTyped = false;

  public Pattern = Pattern;

  public focused = false;
  public touched = false;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('input');

    this.maxlength = !this.maxlength ?
      this.type === 'text' ? 100 : this.type === 'number' ? 8 : 100 : this.maxlength;
  }

  ngOnChanges(changes: SimpleChanges): void {
    const mods = changes.mods && changes.mods.currentValue;
    this.cssClass = this.setClass('input', mods);
  }

  onFocus() {
    this.focused = true;
    this.editContent.nativeElement.focus();

    const length = this.editContent.nativeElement.textContent.length;

    this.setCursor(length);
  }

  onBlur() {
    this.focused = false;
    this.markAsTouched();
  }

  setCursor(pos) {
    const range = document.createRange();
    const selection = window.getSelection();

    if (this.editContent.nativeElement.childNodes[0]) {
      range.setStart(this.editContent.nativeElement.childNodes[0], pos);
      range.collapse(true);
    }

    selection.removeAllRanges();
    selection.addRange(range);
  }

  markAsTouched() {
    if (!this.touched) {
      this.onTouched();
      this.touched = true;
    }

    this.onTouched();
  }

  check(field: any, event: any) {

    if (field.value.length > this.maxlength) {
      event.preventDefault();
      event.stopPropagation();
    }

    const ok = new RegExp(field.pattern).test(field.value);

    if (!ok) {
      // event.preventDefault();
      // event.stopPropagation();
    }
  }
}
