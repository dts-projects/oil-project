import {Component, Input, OnInit} from '@angular/core';
import {ModsService} from '../../../core/services/mods.service';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-contact-box',
  templateUrl: './contact-box.component.html',
  styleUrls: ['./contact-box.component.scss']
})
export class ContactBoxComponent extends BaseComponent implements OnInit {
  @Input() data;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('contact-box');
  }
}
