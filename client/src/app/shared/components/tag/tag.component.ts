import {Component, EventEmitter, forwardRef, Input, OnInit, Output} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {ValueAccessorComponent} from '../value-accessor/value-accessor.component';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TagComponent),
    multi: true
  }]
})
export class TagComponent extends ValueAccessorComponent implements OnInit {
  @Output() delete: EventEmitter<any> = new EventEmitter<any>();
  @Output() change: EventEmitter<any> = new EventEmitter<any>();

  @Input() checked: boolean;

  @Input() type: string;
  @Input() index: any;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('tag');
  }

  onDelete(index) {
    this.delete.emit(index);
  }

  selectTag(e, index: any) {
    e.stopPropagation();
    this.change.emit(index);
  }
}
