import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef, HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {DeviceDetectorService} from 'ngx-device-detector';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {from, fromEvent} from 'rxjs';
import {debounceTime, delay, map, mergeMap} from 'rxjs/operators';
import {ValueAccessorComponent} from '../value-accessor/value-accessor.component';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    }
  ]
})
export class SelectComponent extends ValueAccessorComponent implements OnInit, OnChanges, AfterViewInit {
  @ViewChild('input', {read: ElementRef}) input: ElementRef;
  @ViewChild('inputField', {read: ElementRef}) inputField: ElementRef;

  @Input() id;
  @Input() items;
  @Input() searchType = 'startsWith';

  @Output() change: EventEmitter<any> = new EventEmitter<any>();

  public isSelectOpened = false;
  public fieldValue = null;

  public filteredItems = [];

  public isBreakpointMatched = false;

  constructor(public deviceService: DeviceDetectorService) {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('select');
    this.filteredItems = this.items;
    this.resize();
  }

  ngAfterViewInit(): void {
    const events = ['input', 'focus'];

    from(events)
      .pipe(
        mergeMap(event => fromEvent(this.inputField.nativeElement, event)),
        debounceTime(500),
        map((event: any) => {
          return [event.target, this.filterItems(event.target)];
        }),
        delay(500)
      )
      .subscribe(([input, items]) => {
        if ((items.length === 1)) {
          this.changeValue(items[0].value);
          this.closeSelect();
          input.blur();
        }

        this.filteredItems = items;
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    // if (changes.mods) {
    //   if (changes.mods.currentValue === State.Invalid) {
    //     this.isInvalid = true;
    //   } else {
    //     this.isInvalid = false;
    //   }
    // }
  }

  changeValue(value) {
    super.changeValue(value);
    this.change.emit(value);
    this.setFieldValue();
  }

  writeValue(value) {
    super.writeValue(value);
    this.setFieldValue();
  }

  openSelect() {
    if (this.stateDisabled || this.disabled) {
      return;
    }

    if (this.isBreakpointMatched) {
      this.fieldValue = '';
    } else {
      this.inputField.nativeElement.value = '';
      document.documentElement.classList.add('page--mobile-open');
    }

    this.filteredItems = this.items.filter((item) => item.value);
    this.isSelectOpened = true;
  }

  closeSelect() {
    document.documentElement.classList.remove('page--mobile-open');
    this.isSelectOpened = false;
  }

  setFieldValue() {
    const selectItem = this.items.filter((item) => {
      return item.value === this.value;
    })[0];

    if (selectItem) {
      this.fieldValue = selectItem.name;
    }
  }

  filterItems(input: HTMLInputElement) {
    return this.items.filter((item) => {
      return item.value && (item.name.toLowerCase()[this.searchType](input.value.toLowerCase()));
    });
  }

  formatInputValue(value: string) {
    const targetItem = this.items.filter((item) => {
      return item.value === value;
    })[0];

    return targetItem ? targetItem.name : '';
  }

  @HostListener('window:resize', ['$event'])

  resize() {
    this.isBreakpointMatched =  window.matchMedia(`(min-width: 992px)`).matches;
  }
}
