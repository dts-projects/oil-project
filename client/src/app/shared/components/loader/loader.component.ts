import {Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent extends BaseComponent implements OnInit {
  @Input() color = 'rgba(0, 0, 0, 0.2)';

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('loader');
  }
}
