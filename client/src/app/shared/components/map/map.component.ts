import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ModsService} from '../../../core/services/mods.service';
import {AgmMarker} from '@agm/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent extends BaseComponent implements OnInit {
  @Input() points: Array<any>;
  @Input() latitude: string;
  @Input() longitude: string;
  @Input() zoom: string;
  @Input() disableDefaultUI: boolean;
  @Input() gestureHandling: boolean;
  @Input() scrollwheel: boolean;
  @Input() styles: any;
  @Output() pointClick: EventEmitter<any> = new EventEmitter<any>();

  title = 'AGM project';

  public imagesPath = '/assets/images/components/map/images/';

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('map');
  }

  mapClick($event: MouseEvent) {}

  pointChange($event: AgmMarker) {
    this.pointClick.emit($event.title);
  }
}
