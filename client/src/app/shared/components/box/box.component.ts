import {Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.scss']
})
export class BoxComponent extends BaseComponent implements OnInit {
  @Input() data;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('box');
  }
}
