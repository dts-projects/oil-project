import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { AngularFireModule} from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import {AngularFirestore, AngularFirestoreModule} from '@angular/fire/firestore';
import {environment} from '../environments/environment';
import {ModalsModule} from './modals/modals.module';

import {NotFoundPageModule} from './pages/not-found-page/not-found-page.module';
import {IndexPageModule} from './pages/index-page/index-page.module';

import {registerLocaleData} from '@angular/common';
import ruLocale from '@angular/common/locales/ru';
import {PageHeaderModule} from './layout-components/page-header/page-header.module';
import {PageFooterModule} from './layout-components/page-footer/page-footer.module';
import {SharedModule} from "./shared/shared.module";

registerLocaleData(ruLocale, 'ru');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PageHeaderModule,
    PageFooterModule,
    IndexPageModule,
    NotFoundPageModule,
    SharedModule,
    // IndexPageModule,
    // ModalsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFirestoreModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
