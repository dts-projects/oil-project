import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {SwiperComponent} from 'swiper/angular';
import MediaQuery from '../../../../core/utils/media';

@Component({
  selector: 'app-products-slider',
  templateUrl: './products-slider.component.html',
  styleUrls: ['./products-slider.component.scss']
})
export class ProductsSliderComponent implements OnInit {
  @ViewChild('swiper', { static: false }) swiper?: SwiperComponent;

  @Input() id: string;
  @Input() data: any;
  @Input() config: any;

  constructor() { }

  ngOnInit(): void {

    this.config = {
      slidesPerView: 1,
      spaceBetween: 0,
      centeredSlides: true,
      loop: true,
      breakpointsInverse: true,
      breakpoints: {
        [MediaQuery.SM]: {
          slidesPerView: 2,
          // spaceBetween: 20
        },
        [MediaQuery.MD]: {
          slidesPerView: 3,
          // spaceBetween: 20
        }
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true}
    };
  }

  slideNext() {
    this.swiper.swiperRef.slideNext(100);
  }

  slidePrev() {
    this.swiper.swiperRef.slidePrev(100);
  }
}
