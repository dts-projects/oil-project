import {Component, Input, OnInit, ViewChild} from '@angular/core';
import MediaQuery from '../../../../core/utils/media';
import {SwiperComponent} from "swiper/angular";

@Component({
  selector: 'app-news-slider',
  templateUrl: './news-slider.component.html',
  styleUrls: ['./news-slider.component.scss']
})
export class NewsSliderComponent implements OnInit {
  @ViewChild('swiper', { static: false }) swiper?: SwiperComponent;

  @Input() id: string;
  @Input() data: any;
  @Input() config: any;

  constructor() { }

  ngOnInit(): void {

    this.config = {
      slidesPerView: 1,
      spaceBetween: 0,
      breakpointsInverse: true,
      breakpoints: {
        [MediaQuery.SM]: {
          slidesPerView: 2,
          spaceBetween: 10
        },
        [MediaQuery.MD]: {
          slidesPerView: 2,
          spaceBetween: 20
        }
        ,
        [MediaQuery.LG]: {
          slidesPerView: 3,
          spaceBetween: 20
        }
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true}
    };
  }

  slideNext() {
    this.swiper.swiperRef.slideNext(100);
  }

  slidePrev() {
    this.swiper.swiperRef.slidePrev(100);
  }

}
