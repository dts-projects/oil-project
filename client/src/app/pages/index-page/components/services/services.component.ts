import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {OrderModalComponent} from "../../../../modals/order-modal/order-modal.component";
import {SimpleModalService} from "ngx-simple-modal";

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  @ViewChild('content', {read: ElementRef}) content: ElementRef;

  public currentPanelId = 1;

  constructor(private modalService: SimpleModalService) { }

  ngOnInit(): void {
  }

  showPanel(id) {
    this.currentPanelId = id;
    this.scrollTo();
  }

  showModal(data) {
    this.modalService.addModal(OrderModalComponent, {
      type: 'service',
      title: data.title,
      subtitle: data.subtitle
    });
  }

  scrollTo() {
    this.content.nativeElement.scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  }
}
