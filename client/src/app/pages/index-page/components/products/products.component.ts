import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {


  public data = [
    {
      id: '1',
      img: 'img',
      title: 'АИ 95 / 92 <br /> Дизельное топлливо',
      subtitle: 'АИ 95 / 92. Дизельное топлливо.',
      list: ['Реализация на собственных АЗС', 'Отпуск по топливным картам и ведомостям', 'Доставка до клиента', 'Оптовая продажа'],
      products: [{value: 50, name: 'АИ 92 - 50 р / литр'}, {value: 70, name: 'АИ 95 - 60 р / литр'}, {value: 80, name: 'Дизельное топливо - 80 р / литр'}],
    },
    // {
    //   id: '2',
    //   img: 'img',
    //   title: 'Дизельное топливо',
    //   subtitle: 'от 60р за 1 литр.',
    //   list: ['Реализация на собственных АЗС', 'Отпуск по топливным картам и ведомостям', 'Доставка до клиента', 'Оптовая продажа'],
    //   price: 60
    // },
    {
      id: '3',
      img: 'img',
      title: 'Отопительное топливо',
      subtitle: 'от 70р за 1 литр.',
      list: ['Реализация на собственных АЗС', 'Отпуск по топливным картам и ведомостям', 'Доставка до клиента', 'Оптовая продажа'],
      price: 70
    },
    {
      id: '4',
      img: 'img',
      title: 'Битум',
      subtitle: 'от 80р за 1 литр.',
      list: ['Реализация на собственных АЗС', 'Отпуск по топливным картам и ведомостям', 'Доставка до клиента', 'Оптовая продажа'],
      price: 80
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
