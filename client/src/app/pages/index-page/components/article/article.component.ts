import {Component, Input, OnInit} from '@angular/core';
import {SimpleModalService} from 'ngx-simple-modal';
import {NewsModalComponent} from '../../../../modals/news-modal/news-modal.component';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  @Input() data: any;

  public imagesPath = '/assets/images/pages/index-page/components/article/images';

  constructor(private modalService: SimpleModalService) { }

  ngOnInit(): void {
  }

  showModal(e, data) {
    e.preventDefault();

    this.modalService.addModal(NewsModalComponent, {
      id: '666',
      title: data.title,
      text: data.text,
      date: data.date
    });
  }

  trimText(text) {
    return text.slice(0, 200) + '...';
  }
}
