import {Component, Input, OnInit} from '@angular/core';
import {SimpleModalService} from 'ngx-simple-modal';
import {AlertModalComponent} from '../../../../modals/alert-modal/alert-modal.component';
import {OrderModalComponent} from "../../../../modals/order-modal/order-modal.component";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() data: any;

  constructor(private modalService: SimpleModalService) { }

  ngOnInit(): void {
  }

  showModal(data) {
    // this.modalService.addModal(OrderModalComponent, {
    //   title,
    //   list: of(cities)
    // });

    this.modalService.addModal(OrderModalComponent, {
      type: 'product',
      title: data.title,
      subtitle: 'Безналичный расчет с НДС, без НДС',
      products: data.products,
      price: data.price
    });
  }

}
