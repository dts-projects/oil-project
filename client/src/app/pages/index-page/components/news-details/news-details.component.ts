import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.scss']
})
export class NewsDetailsComponent implements OnInit {
  @Input() data: any;

  public imagesPath = '/assets/images/examples';

  constructor() { }

  ngOnInit(): void {
  }

}
