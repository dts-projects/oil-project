import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {SwiperComponent} from "swiper/angular";
import MediaQuery from "../../../../core/utils/media";
import {OrderModalComponent} from "../../../../modals/order-modal/order-modal.component";
import {SimpleModalService} from "ngx-simple-modal";

@Component({
  selector: 'app-main-slider',
  templateUrl: './main-slider.component.html',
  styleUrls: ['./main-slider.component.scss']
})
export class MainSliderComponent implements OnInit {
  @ViewChild('swiper', { static: false }) swiper?: SwiperComponent;

  @Input() id: string;
  @Input() config: any;


  public data = [
    {
      type: 'exchange',
      title: 'Закупка нефтепродуктов на товарно-сырьевой бирже ',
      text: 'Перевозка опасных грузов Перевозка опасных грузов Перевозка опасных грузов Перевозка опасных грузов Перевозка опасных грузов'
    },
    {
      type: 'sale',
      title: 'Оптовая и розничная реализация нефтепродуктов',
      text: 'Закупка нефтепродуктов на бирже Закупка нефтепродуктов на бирже Закупка нефтепродуктов на бирже Закупка нефтепродуктов на бирже'
    },
    {
      type: 'transit',
      title: 'Транспортны услуги по перевозке опасных грузов',
      text: 'Реализация с нефтебазы Реализация с нефтебазы  Реализация с нефтебазы Реализация с нефтебазы Реализация с нефтебазы'
    }
  ];

  constructor(private modalService: SimpleModalService) { }

  ngOnInit(): void {

    this.config = {
      slidesPerView: 1,
      spaceBetween: 0,
      centeredSlides: true,
      loop: true,
      breakpointsInverse: true,
      breakpoints: {
        // [MediaQuery.S]: {
        //   slidesPerView: 2,
        //   // spaceBetween: 20
        // },
        // [MediaQuery.SM]: {
        //   slidesPerView: 2,
        //   // spaceBetween: 20
        // },
        // [MediaQuery.MD]: {
        //   slidesPerView: 3,
        //   // spaceBetween: 20
        // }
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true}
    };
  }

  slideNext() {
    this.swiper.swiperRef.slideNext(100);
  }

  slidePrev() {
    this.swiper.swiperRef.slidePrev(100);
  }

  showModal(data) {
    this.modalService.addModal(OrderModalComponent, {
      type: 'service',
      title: data.title,
      subtitle: data.subtitle
    });
  }
}
