export interface Select {
  value: string;
  name: string;
  data?: any;
}
