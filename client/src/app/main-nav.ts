export default [
  {link: 'services', name: 'Услуги'},
  {link: 'goods', name: 'Нефтепродукты'},
  {link: 'news', name: 'Новости'},
  {link: 'about', name: 'О компании'},
  {link: 'contacts', name: 'Контакты'}
];
