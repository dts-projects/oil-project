import {AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import { LocalStorageService } from 'src/app/core/services/local-storage.service';
import {mainNav, topNav} from '../../footer-nav';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-page-footer',
  templateUrl: './page-footer.component.html',
  styleUrls: ['./page-footer.component.scss']
})
export class PageFooterComponent implements OnInit, AfterViewInit {
  @ViewChild('footer', {read: ElementRef}) nav: ElementRef;

  currentFragment = '';

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.currentFragment = this.route.snapshot.fragment;

    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        const target = this.router.parseUrl(event.url).fragment;
        this.currentFragment = target;

        if (target) {
          this.scrollToSection(target);
        }
      }
    });
  }

  ngAfterViewInit(): void {

    setTimeout(() => {
      this.scrollToSection(this.currentFragment);
    }, 0);
  }

  @HostListener('window:scroll', ['$event'])

  scroll(event) {
    this.removeFragment();
  }

  scrollToSection(target) {
    const section = document.getElementById(target);

    if (section) {
      section.scrollIntoView({
        behavior: 'smooth'
      });
    }
  }

  // setInverseMenu() {
  //   if (this.nav.nativeElement.getBoundingClientRect().top < -30) {
  //     this.isInverse = true;
  //   } else {
  //     this.isInverse = false;
  //   }
  // }

  removeFragment() {
    if (this.nav.nativeElement.getBoundingClientRect().top === 0) {
      this.router.navigate(['.'], {relativeTo: this.route, queryParams: []});
    }
  }
}
