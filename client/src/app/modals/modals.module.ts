import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DefaultSimpleModalOptionConfig, defaultSimpleModalOptions, SimpleModalModule} from 'ngx-simple-modal';
import {SharedModule} from '../shared/shared.module';
import {SignInModalComponent} from './sign-in-modal/sign-in-modal.component';
import {LoginModalComponent} from './login-modal/login-modal.component';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { AlertModalComponent } from './alert-modal/alert-modal.component';
import { MapModalComponent } from './map-modal/map-modal.component';
import { OrderModalComponent } from './order-modal/order-modal.component';
import { NewsModalComponent } from './news-modal/news-modal.component';
import {IndexPageModule} from "../pages/index-page/index-page.module";

@NgModule({
    declarations: [
        LoginModalComponent,
        SignInModalComponent,
        ConfirmModalComponent,
        AlertModalComponent,
        MapModalComponent,
        OrderModalComponent,
        NewsModalComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        SimpleModalModule.forRoot({container: document.body}),
        IndexPageModule
    ],
    exports: [
        SimpleModalModule
    ],
    providers: [
        {
            provide: DefaultSimpleModalOptionConfig,
            useValue: { ...defaultSimpleModalOptions, ...{ closeOnEscape: true, closeOnClickOutside: true, wrapperClass: 'in show' } }
        },
    ]
})
export class ModalsModule { }
