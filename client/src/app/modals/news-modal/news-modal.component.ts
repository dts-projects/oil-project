import {Component, OnDestroy, OnInit} from '@angular/core';
import {SimpleModalComponent} from 'ngx-simple-modal';

@Component({
  selector: 'app-news-modal',
  templateUrl: './news-modal.component.html',
  styleUrls: ['./news-modal.component.scss']
})
export class NewsModalComponent extends SimpleModalComponent<null, null> implements OnInit, OnDestroy {
  public title: string;
  public text: string;
  public date: string;

  constructor() {
    super();
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
  }
}
