import {Component, OnDestroy, OnInit} from '@angular/core';
import {SimpleModalComponent} from 'ngx-simple-modal';

interface CoordsModel {
  geo_x: string;
  geo_y: string;
}

interface OfficeModel {
  address: string;
  coords: CoordsModel;
}

interface MapModel {
  office: OfficeModel;
}

@Component({
  selector: 'app-map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.scss']
})
export class MapModalComponent  extends SimpleModalComponent<null, null> implements OnInit, OnDestroy, MapModel {
  public office: OfficeModel;

  constructor() {
    super();
  }

  ngOnInit(): void {
    console.log('this.office', this.office);
  }

  ngOnDestroy(): void {
  }

}
