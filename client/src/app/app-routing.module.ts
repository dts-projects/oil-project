import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules, ExtraOptions} from '@angular/router';
import {IndexPageComponent} from './pages/index-page/index-page.component';
import {NotFoundPageComponent} from './pages/not-found-page/not-found-page.component';

const routerOptions: ExtraOptions = {
  // scrollPositionRestoration: 'enabled',
  // anchorScrolling: 'enabled',
  scrollOffset: [60, 60],
  // scrollPositionRestoration: 'enabled',
  // preloadingStrategy: PreloadAllModules
  // onSameUrlNavigation: 'reload'
};

const routes: Routes = [
  {path: '', component: IndexPageComponent, data: { title: 'Главная' }},
  {path: '**', component: NotFoundPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
