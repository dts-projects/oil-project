// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api_url: 'https://api.busbox.guru',
  api_key: '8aab09f6-c5b3-43be-8895-153ea164984e',
  firebaseConfig: {
    apiKey: 'AIzaSyA-RQfT8UV22h7-y0aFL22HOxcmsZEYuM4',
    authDomain: 'oil-project-34d3c.firebaseapp.com',
    projectId: 'oil-project-34d3c',
    storageBucket: 'oil-project-34d3c.appspot.com',
    messagingSenderId: '233584341905',
    appId: '1:233584341905:web:617e178bacb2a0391a6030'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
